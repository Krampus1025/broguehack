#include <gl/glut.h>
#include "Hook.h"
#include "Draw.h"
#include "World.h"
#include "HackMenu.h"
#include <algorithm>
#include <functional>

Menu::~Menu()
{
	for (auto it : tabs)
	{
		delete it;
	}

	tabs.clear();
}

void Menu::drawContent(Tab * tab)
{
	m_draw.string(0, 23 * 2, 1, 1, 1, 1, GLUT_BITMAP_TIMES_ROMAN_24, "%s", tab->m_contents.c_str());
}

void Menu::drawTabs(Tab * tab, int position)
{
	//Draw the main tab
	m_draw.box(0, 0, 500, 25, 0.4f, 0.4f, 0.4f);
	//Draw the tab
	m_draw.box((500 / tabs.size()) * position, 0, 500 / tabs.size(), 25, 0.8f, 0.8f, 0.8f);
	//Label the tab
	m_draw.string((500 / tabs.size()) * position + 5, 20 * 1, 1, 1, 1, 1, GLUT_BITMAP_TIMES_ROMAN_24, "%s", tab->m_title.c_str());

	//Draw outline for good measure
	m_draw.outlinedLine(1, 0, 25, (500 / tabs.size()) * position, 25, 1.0f, 1.0f, 1.0f);
	m_draw.outlinedLine(1, (500 / tabs.size()) * position, 0, (500 / tabs.size()) * position, 25, 1.0f, 1.0f, 1.0f);
	m_draw.outlinedLine(1, (500 / tabs.size()) * position + 500 / tabs.size(), 25, 500, 25, 1.0f, 1.0f, 1.0f);
	m_draw.outlinedLine(1, (500 / tabs.size()) * position + 500 / tabs.size(), 0, (500 / tabs.size()) * position + 500 / tabs.size(), 25, 1.0f, 1.0f, 1.0f);
}

void Menu::drawMenu()
{
	drawTabs(tabs[m_curTab], m_curTab);
	drawContent(tabs[m_curTab]);
}

void Menu::addTab(std::string title, std::string contents)
{
	tabs.push_back(new Tab(title, contents));
}

void Menu::setTabContent(std::string title, char const*const pFmtText, ...)
{
	//Allocate memory for a temporary result string. You could probably do much less
	char *const pTemp = new char[5000];

	//Put all of the text into pTemp
	va_list vaList;
	va_start(vaList, pFmtText);
	int const TextLen = vsprintf(pTemp, pFmtText, vaList);
	va_end(vaList);

	for (auto it : tabs)
	{
		if (it->m_title == title)
		{
			it->m_contents.clear();
			it->m_contents.resize(TextLen + 1);

			for (int i = 0; i < TextLen; i++)
			{
				it->m_contents[i] = pTemp[i];
			}
		}
	}

	//Prevent memory leaks
	delete[] pTemp;
}

void Menu::cycleState()
{
	for (int i = 0; i != tabs.size(); i++)
	{
		if (i == tabs.size() - 1)
		{
			m_curTab = 0;
			break;
		}

		else if (i == m_curTab)
		{
			m_curTab++;
			break;
		}
	}
}