#include <vector>
#include <string>

class Tab;

class Menu
{
	Draw m_draw;
	std::vector<Tab*> tabs;

	int m_curTab;

	void drawContent(Tab * tab);
	void drawTabs(Tab * tab, int position);

public:
	Menu() : m_curTab(0) { }
	~Menu();

	void drawMenu();
	void addTab(std::string title, std::string contents);
	void setTabContent(std::string title, char const*const pFmtText, ...);
	void cycleState();
};

class Tab
{
public:
	Tab(std::string title, std::string contents) : m_title(title), m_contents(contents) { }

	std::string m_title;
	std::string m_contents;
};