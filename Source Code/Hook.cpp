#include <iostream>
#include <algorithm>
#include <vector>
#include "Hook.h"

Hook::Hook(void * hkAddy, void * hkFnctAddy, DWORD len)
{
	m_isHooked = false;
	m_hkAddy = hkAddy;
	m_len = len;
	m_hkFnctAddy = hkFnctAddy;
	m_restoreBytes = new BYTE[200];
}

Hook::~Hook()
{
	delete[] m_restoreBytes;
}

trpHook::trpHook(void * hkAddy, void * hkFnctAddy, DWORD len) :
Hook(hkAddy, hkFnctAddy, len)
{ }

void * trpHook::CreateDetour()
{
	void * pTrp;
	DWORD oldProtect, Bkup, relativeAddy;

	if (m_hkAddy == NULL || m_len < 5)
	{
		return nullptr;
	}

	if (!VirtualProtect(m_hkAddy, m_len, PAGE_EXECUTE_READWRITE, &oldProtect))
	{
		return nullptr;
	}

	//Allocate a spot of memory for pTrp
	pTrp = VirtualAlloc(0, m_len + 5, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	//Copy the bytes into the beginning of pTrp for when we're returning.
	memcpy(pTrp, m_hkAddy, m_len);
	//Copy the bytes into m_restoreBytes for when we want to undo the hook
	memcpy(m_restoreBytes, m_hkAddy, m_len);
	//NOP out the start of the function
	memset(m_hkAddy, 0x90, m_len);

	/*PLACING OUR JUMP AT THE ORIGINAL FUNCTION*/

	//Get relative address, place jmp, put relative address on next 4 bytes
	relativeAddy = ((DWORD)m_hkFnctAddy - (DWORD)m_hkAddy) - 5;
	*(BYTE*)m_hkAddy = 0xE9;
	*(DWORD*)((DWORD)m_hkAddy + 0x1) = relativeAddy;

	/*PLACING OUR JUMP AT THE TRAMPOLINE*/

	//Get relative address, place jmp, put relative address on next 4 bytes
	DWORD relAddy = ((DWORD)m_hkAddy - (DWORD)pTrp) - 5;
	*(BYTE*)((DWORD)pTrp + m_len) = 0xE9;
	*(DWORD*)((DWORD)pTrp + m_len + 0x1) = relAddy;

	//Restore whatever protection there was
	if (!VirtualProtect(m_hkAddy, m_len, oldProtect, &Bkup))
	{
		return nullptr;
	}

	return pTrp;
}

bool trpHook::Restore()
{
	DWORD oldProtect, Bkup;

	if (m_restoreBytes == NULL)
	{
		return false;
	}

	if (!VirtualProtect(m_hkAddy, m_len, PAGE_EXECUTE_READWRITE, &oldProtect))
	{
		return false;
	}

	memcpy(m_hkAddy, m_restoreBytes, m_len);

	if (!VirtualProtect(m_hkAddy, m_len, oldProtect, &Bkup))
	{
		return false;
	}

	return true;
}

void * trpHook::ToggleHook(void * oFunct)
{
	if (m_isHooked == false)
	{
		void * pTemp = CreateDetour();

		if (pTemp != nullptr)
		{
			m_isHooked = true;
			return pTemp;
		}

		else
		{
			MessageBoxA(NULL, "Failed to hook", "ERROR", MB_OK);
		}
	}

	else if (m_isHooked == true)
	{
		if (!Restore())
		{
			MessageBoxA(NULL, "Either failed to restore or bytes are already restored", "ERROR", MB_OK);
		}

		else
		{
			m_isHooked = false;
			VirtualFree(oFunct, m_len + 5, MEM_DECOMMIT);
			return nullptr;
		}
	}
}

void trpHook::Unload()
{
	if (m_isHooked == true)
	{
		if (!Restore())
		{
			MessageBoxA(NULL, "Either failed to restore or bytes are already restored", "ERROR", MB_OK);
		}

		else
		{
			m_isHooked = false;
		}
	}
}

plainHook::plainHook(void * hkAddy, void * hkFnctAddy, DWORD len) :
Hook(hkAddy, hkFnctAddy, len)
{ }

plainHook::plainHook(char * module, void * hkFnctAddy, char * pattern, char * mask, DWORD len) :
m_module(module), m_pattern(pattern), m_mask(mask),
Hook((BYTE*)FindPattern(), hkFnctAddy, len)
{ }

bool plainHook::CreateDetour()
{
	DWORD oldProtect, Bkup, relativeAddy;

	if (m_hkAddy == NULL || m_len == NULL)
	{
		return false;
	}

	if (m_len < 5)
	{
		return false;
	}

	if (!VirtualProtect(m_hkAddy, m_len, PAGE_EXECUTE_READWRITE, &oldProtect))
	{
		return false;
	}

	memcpy(m_restoreBytes, m_hkAddy, m_len);
	memset(m_hkAddy, 0x90, m_len);

	//Get relative address, place jmp, put relative address on next 4 bytes
	relativeAddy = ((DWORD)m_hkFnctAddy - (DWORD)m_hkAddy) - 5;
	*(BYTE*)m_hkAddy = 0xE9;
	*(DWORD*)((DWORD)m_hkAddy + 0x1) = relativeAddy;

	if (!VirtualProtect(m_hkAddy, m_len, oldProtect, &Bkup))
	{
		return false;
	}

	return true;
}

bool plainHook::Restore()
{
	DWORD oldProtect, Bkup;

	if (m_restoreBytes == NULL)
	{
		return false;
	}

	if (!VirtualProtect(m_hkAddy, m_len, PAGE_EXECUTE_READWRITE, &oldProtect))
	{
		return false;
	}

	memcpy(m_hkAddy, m_restoreBytes, m_len);

	if (!VirtualProtect(m_hkAddy, m_len, oldProtect, &Bkup))
	{
		return false;
	}

	return true;
}

DWORD plainHook::FindPattern()
{
	//Credits to Fleep for this information
	MODULEINFO modInfo = { 0 };

	//Returns a handle to the specified module
	HMODULE hModule = GetModuleHandle(m_module);

	//Places all of the needed information in modInfo
	GetModuleInformation(GetCurrentProcess(), hModule, &modInfo, sizeof(MODULEINFO));

	//The load address of the module
	DWORD base = (DWORD)modInfo.lpBaseOfDll;
	//The size of the linear space that the module occupies, in bytes.
	DWORD size = (DWORD)modInfo.SizeOfImage;

	//Our vector of bools
	std::vector<bool> isFound(strlen(m_mask), false);
	auto iter = isFound.end();
	iter--;

	//Our pattern length for looping
	DWORD patternLength = (DWORD)strlen(m_mask);

	for (DWORD i = 0; i < size - patternLength; i++)
	{
		bool found = true;

		for (DWORD j = 0; j < patternLength; j++)
		{
			//If at any point one of them is false, found will become false
			if ((m_mask[j] == '?') || (m_pattern[j] == *(char*)(base + i + j)))
			{
				isFound[j] = true;
			}

			else
			{
				isFound[j] = false;
				break;
			}
		}

		//Check and make sure every element is true
		for (auto c : isFound)
		{
			//If not, we continue looping
			if (c == false)
			{
				found = false;
			}
		}

		if (found)
		{
			//Returns the base of the module + the offset
			return base + i;
		}
	}

	return NULL;
}

void plainHook::ToggleHook()
{
	if (m_isHooked == false)
	{
		if (!CreateDetour())
		{
			MessageBoxA(NULL, "Failed to hook", "ERROR", MB_OK);
		}

		else
		{
			m_isHooked = true;
		}
	}
	
	else if (m_isHooked == true)
	{
		if (!Restore())
		{
			MessageBoxA(NULL, "Either failed to restore or bytes are already restored", "ERROR", MB_OK);
		}

		else
		{
			m_isHooked = false;
		}
	}
}

void plainHook::Unload()
{
	if (m_isHooked == true)
	{
		if (!Restore())
		{
			MessageBoxA(NULL, "Either failed to restore or bytes are already restored", "ERROR", MB_OK);
		}

		else
		{
			m_isHooked = false;
		}
	}
}