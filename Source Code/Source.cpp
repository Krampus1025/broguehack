//The mass amount of includes needed
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/glut.h>
#include <gl/glfw3.h>
#include "Hook.h"
#include "World.h"
#include "Draw.h"
#include "HackMenu.h"

//Takes the program flow from Engine and starts the loop
void main(GLFWwindow* window);
//Frees our DLL from the program it is injected into
void freeDLL();
//Prepares the frame for drawing
void prepFrame();
//Our hook function. Sets the stealth range to 0 so we can sneak passed most monsters
void stealthHkFunct();
//Creates the OpenGL window and starts the needed loops
DWORD WINAPI Engine(LPVOID param);

//Making globals look semi-bearable
struct hook
{
	DWORD hookAddy, returnAddy, variable;
}stealth;

void main(GLFWwindow* window)
{
	//Our hooking information for the stealth hook
	stealth.hookAddy = 0x4655B5;
	stealth.variable = (DWORD)(0x501C92);
	stealth.returnAddy = 0x4655BB;
	//Create the stealth hook
	plainHook stealthHook((void*)stealth.hookAddy, stealthHkFunct, 6);

	//Contains information on the player
	World * world = (World*)0x445188;

	//Our hack menu
	Menu menu;
	menu.addTab("Stats", "Dynamic Value");
	menu.addTab("Hacks", "Dynamic Value");
	menu.addTab("Help", "Cycle Tabs: F1\nToggle Stealth Hack: F2");
	menu.addTab("Credits", "Credits:\nKrampus - Creator\nVisit Guidedhacking.com for more information!");

	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{
		prepFrame();

		//Draw the menu for our hack
		menu.setTabContent("Stats", "Health: %d\n"
									"Gold: %d\n"
									"Strength: %d\n"
									"Number Of Moves: %d\n"
									"Player Position: (%d, %d)\n"
									"Mouse Position: (%d, %d)\n",
									world->Player->health,
									world->Player->gold,
									world->Player->strength,
									world->Player->numberOfMoves,
									world->Player->posX, world->Player->posY,
									world->Player->mouseX, world->Player->mouseY);

		menu.setTabContent("Hacks", "Stealth Hack: %s", stealthHook.Hooked() ? "On" : "Off");
		menu.drawMenu();

		//If you press F1, it cycles through the hack window tabs
		if (GetAsyncKeyState(VK_F1) & 1)
		{
			menu.cycleState();
		}

		//If you press F2, it toggles the stealthHook hack
		if (GetAsyncKeyState(VK_F2) & 1)
		{
			stealthHook.ToggleHook();
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	//Make sure the hook isn't on when we exit, which could lead to a crash
	stealthHook.Unload();
}

void freeDLL()
{
	HMODULE myModule = NULL;
	while (!myModule)
	{
		myModule = GetModuleHandleA("Brogue Hack.dll");
	}
	FreeLibraryAndExitThread(myModule, NULL);
}

void prepFrame()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, 500, 300);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, 500, 300, 0.0f, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//Our hook function. Sets the stealth range to 0 so we can sneak passed most monsters
__declspec(naked) void stealthHkFunct()
{
	__asm
	{
		push eax
		mov eax, [stealth.variable]
		mov[eax], 0
		pop eax
		jmp[stealth.returnAddy]
	}
}

//Main thread
DWORD WINAPI Engine(LPVOID param)
{
	glfwInit();

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 1);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_RESIZABLE, false);

	GLFWwindow* window;
	window = glfwCreateWindow(500, 300, "BROGUEHACK", NULL, NULL);
	glfwMakeContextCurrent(window);

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.8, 0.8, 0.8, 1.0);
	main(window);
	freeDLL();

	return NULL;
}

//Our entry point
BOOL WINAPI DllMain(HINSTANCE instance, DWORD reason, LPVOID reserved)
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
		CreateThread(0, 0, Engine, 0, 0, 0);
		DisableThreadLibraryCalls(instance);
		AllocConsole();
		AttachConsole(GetProcessId(instance));
		break;
	case DLL_THREAD_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_DETACH:
		FreeConsole();
		break;
	}

	return TRUE;
}